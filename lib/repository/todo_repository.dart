import 'dart:convert';

import 'package:flutter_with_ui_from_fe/models/todo.dart';
import 'package:http/http.dart' as http;

class TodoRepository {
  Future<List<Todo>> getAll() async {
    const url =
        'https://staging.lawrencesmoke.ca/wp-json/pos-api/v1/categories?page=1';
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as List;
      final result = json.map((e) {
        return Todo(
            id: e['id'],
            name: e['name'],
            slug: e['slug'],
            desc: e['desc'],
            count: e['count'],
            children: e['children'],
            image: e['image']);
      }).toList();
      return result;
    } else {
      throw "Something went wrong code ${response.statusCode}!";
    }
  }
}
