import 'dart:convert';

import 'package:flutter_with_ui_from_fe/models/category.dart';
import 'package:flutter_with_ui_from_fe/models/detailsCategory.dart';
import 'package:http/http.dart' as http;

class CategoryRepository {
  Future<List<Category>> getAllCategory() async {
    const url = 'https://www.themealdb.com/api/json/v1/1/categories.php';
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final Map<String, dynamic> jsonResponse = jsonDecode(response.body);
      final List<dynamic> categoriesJson = jsonResponse['categories'];

      final result = categoriesJson.map((e) {
        return Category(
          idCategory: e['idCategory'],
          strCategory: e['strCategory'],
          strCategoryThumb: e['strCategoryThumb'],
          strCategoryDescription: e['strCategoryDescription'],
        );
      }).toList();

      return result;
    } else {
      throw "Something went wrong code ${response.statusCode}!";
    }
  }

  Future<List<DetailsCategory>> getFoodByCategory(String category) async {
    final url =
        'https://www.themealdb.com/api/json/v1/1/filter.php?c=$category'; // Include the category parameter in the URL
    final uri = Uri.parse(url);
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final Map<String, dynamic> jsonResponse = jsonDecode(response.body);
      final List<dynamic> categoriesJson = jsonResponse['meals'];

      final result = categoriesJson.map((e) {
        return DetailsCategory(
          strMeal: e['strMeal'],
          strMealThumb: e['strMealThumb'],
          idMeal: e['idMeal'],
        );
      }).toList();

      return result;
    } else {
      throw "Something went wrong code ${response.statusCode}!";
    }
  }
}
