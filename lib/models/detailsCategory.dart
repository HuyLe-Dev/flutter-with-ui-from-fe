class DetailsCategory {
  final String strMeal;
  final String strMealThumb;
  final String idMeal;

  DetailsCategory({
    required this.strMeal,
    required this.strMealThumb,
    required this.idMeal,
  });
}
