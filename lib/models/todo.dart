class Todo {
  final int id;
  final String name;
  final String slug;
  final String desc;
  final String count;
  final String children;
  final String image;
  Todo({
    required this.id,
    required this.name,
    required this.slug,
    required this.desc,
    required this.count,
    required this.children,
    required this.image,
  });
}
