import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_with_ui_from_fe/category/cubit/category_state.dart';
import 'package:flutter_with_ui_from_fe/category/cubit/category_cubit.dart';

class CategoryArguments {
  final String categoryName;
  final String categoryThumb;

  CategoryArguments({
    required this.categoryName,
    required this.categoryThumb,
  });
}

class CategoryPage extends StatefulWidget {
  const CategoryPage({super.key});

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final cubit = context.read<CategoryCubit>();
      cubit.fetchCategory();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.deepOrange.shade100,
        body: BlocBuilder<CategoryCubit, CategoryState>(
            builder: ((context, state) {
          if (state is InitCategoryState || state is LoadingCategoryState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is ResponseCategoryState) {
            final categories = state.categories;
            return Column(
              children: [
                const BigCard(),
                Expanded(
                  child: ListView.builder(
                    itemCount: categories.length,
                    itemBuilder: ((context, index) {
                      final category = categories[index];
                      return Column(
                        children: [
                          const Divider(), // Add a Divider between ListTiles
                          GestureDetector(
                            onTap: () {
                              final arguments = CategoryArguments(
                                categoryName: category.strCategory,
                                categoryThumb: category.strCategoryThumb,
                              );
                              Navigator.pushNamed(
                                context,
                                '/details',
                                arguments: arguments,
                              );
                            },
                            child: ListTile(
                              title: Text(
                                category.strCategory,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              subtitle: Row(
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      'Description: ${category.strCategoryDescription}',
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Expanded(
                                    child: Image.network(
                                      category.strCategoryThumb,
                                      width: 100,
                                      height: 100,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      );
                    }),
                  ),
                )
              ],
            );
          } else if (state is ErrorCategoryState) {
            return Center(child: Text(state.message));
          }
          return Center(child: Text(state.toString()));
        })));
  }
}

class BigCard extends StatelessWidget {
  const BigCard({super.key});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var style = theme.textTheme.displaySmall!.copyWith(
      color: theme.colorScheme.onPrimary,
    );

    return Card(
      color: theme.colorScheme.primary,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Text(
          "Menu Today",
          style: style,
          semanticsLabel: "Menu Today",
        ),
      ),
    );
  }
}
