import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_with_ui_from_fe/category/cubit/details_category_cubit.dart';
import 'package:flutter_with_ui_from_fe/category/cubit/details_category_state.dart';

class DetailsPage extends StatefulWidget {
  final String categoryName;
  final String categoryThumb;

  // Constructor to receive data from the previous page
  DetailsPage({
    required this.categoryName,
    required this.categoryThumb,
  });

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final cubit = context.read<DetailsCategoryCubit>();
      cubit.fetchDetailsCategory(widget.categoryName);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('${widget.categoryName} Menu'),
          backgroundColor: Colors.white30,
        ),
        body: Scaffold(
          backgroundColor: Colors.deepOrange.shade100,
          body: BlocBuilder<DetailsCategoryCubit, DetailsCategoryState>(
            builder: (context, state) {
              if (state is InitDetailsCategoryState ||
                  state is LoadingDetailsCategoryState) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is ResponseDetailsCategoryState) {
                final detailsCategories = state.detailsCategories;
                return Column(
                  children: [
                    Image.network(
                      widget.categoryThumb,
                      width: 100,
                      height: 100,
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: detailsCategories.length,
                        itemBuilder: ((context, index) {
                          final detailsCategory = detailsCategories[index];
                          final Random random = Random();
                          final int randomPrice = 10 +
                              random.nextInt(
                                  91); // Generates a random double between 10 and 100

                          return Column(
                            children: [
                              const Divider(), // Add a Divider between ListTiles
                              GestureDetector(
                                child: ListTile(
                                  title: Text(
                                    detailsCategory.strMeal,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  subtitle: Row(
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          'Price: \$$randomPrice',
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                      Expanded(
                                        child: Image.network(
                                          detailsCategory.strMealThumb,
                                          width: 100,
                                          height: 100,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                        }),
                      ),
                    )
                  ],
                );
              } else if (state is ErrorDetailsCategoryState) {
                return Center(child: Text(state.message));
              }
              return Center(child: Text(state.toString()));
            },
          ),
        ));
  }
}
