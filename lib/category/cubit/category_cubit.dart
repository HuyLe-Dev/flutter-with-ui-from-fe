import 'package:flutter_with_ui_from_fe/category/cubit/category_state.dart';
import 'package:flutter_with_ui_from_fe/category/cubit/details_category_state.dart';
import 'package:flutter_with_ui_from_fe/repository/category_repository.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

class CategoryCubit extends Cubit<CategoryState> {
  final CategoryRepository _repository;
  CategoryCubit(this._repository) : super(InitCategoryState());

  Future<void> fetchCategory() async {
    emit(LoadingCategoryState());
    try {
      final response = await _repository.getAllCategory();
      emit(ResponseCategoryState(response));
    } catch (e) {
      emit(ErrorCategoryState(e.toString()));
    }
  }
}
