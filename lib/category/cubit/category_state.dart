import 'package:flutter_with_ui_from_fe/models/category.dart';

abstract class CategoryState {}

class InitCategoryState extends CategoryState {}

class LoadingCategoryState extends CategoryState {}

class ErrorCategoryState extends CategoryState {
  final String message;
  ErrorCategoryState(this.message);
}

class ResponseCategoryState extends CategoryState {
  final List<Category> categories;
  ResponseCategoryState(this.categories);
}
