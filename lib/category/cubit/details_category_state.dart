import 'package:flutter_with_ui_from_fe/models/detailsCategory.dart';

abstract class DetailsCategoryState {}

class InitDetailsCategoryState extends DetailsCategoryState {}

class LoadingDetailsCategoryState extends DetailsCategoryState {}

class ErrorDetailsCategoryState extends DetailsCategoryState {
  final String message;
  ErrorDetailsCategoryState(this.message);
}

class ResponseDetailsCategoryState extends DetailsCategoryState {
  final List<DetailsCategory> detailsCategories;
  ResponseDetailsCategoryState(this.detailsCategories);
}
