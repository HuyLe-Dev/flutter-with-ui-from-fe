import 'package:flutter_with_ui_from_fe/category/cubit/category_state.dart';
import 'package:flutter_with_ui_from_fe/category/cubit/details_category_state.dart';
import 'package:flutter_with_ui_from_fe/repository/category_repository.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

class DetailsCategoryCubit extends Cubit<DetailsCategoryState> {
  final CategoryRepository _repository;
  DetailsCategoryCubit(this._repository) : super(InitDetailsCategoryState());

  Future<void> fetchDetailsCategory(String query) async {
    emit(LoadingDetailsCategoryState());
    try {
      final response = await _repository.getFoodByCategory(query);
      emit(ResponseDetailsCategoryState(response));
    } catch (e) {
      emit(ErrorDetailsCategoryState(e.toString()));
    }
  }
}
